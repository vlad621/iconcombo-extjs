/**
 * This view is an example list of people.
 */
Ext.define('TestApp.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'TestApp.store.Personnel'
    ],

    title: 'Personnel',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name' },
        { text: 'Email', dataIndex: 'email', flex: 1 },
        { text: 'Phone', dataIndex: 'phone', flex: 1 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});

var iconList = Ext.create('Ext.data.Store', {
    data: [
        { 'icon': 'fa fa-comment', 'text': 'comments' },
        { 'icon': 'fa fa-crop', 'text': 'crop' },
        { 'icon': 'fa fa-diamond', 'text': 'diamond' },
        { 'icon': 'fa fa-cube', 'text': 'cube' }
    ]
})

Ext.define('TestApp.view.main.Combo', {
    extend: 'Ext.form.ComboBox',
    xtype: 'mainCombo',

    store: iconList,

    fieldLabel: 'Icon',
    displayField: 'text',
    valueField: 'text',
    queryMode: 'local',

    displayTpl: Ext.create('Ext.XTemplate', [
        '<tpl for=".">',
        '<div style="font-size:120%; padding:7px"><i class="{icon}"></i> {text}&nbsp;</div>',
        '</tpl>'
    ]),

    fieldSubTpl: [
        '<div class="{hiddenDataCls}" role="presentation"></div>',
        '<div id="{id}" type="{type}" ',
        '<tpl if="size">size="{size}" </tpl>',
        '<tpl if="tabIdx">tabIndex="{tabIdx}" </tpl>',
        'class="{fieldCls} {typeCls}" autocomplete="off"></div>',
        '<div id="{cmpId}-triggerWrap" class="{triggerWrapCls}" role="presentation">',
        '{triggerEl}',
        '<div class="{clearCls}" role="presentation"></div>',
        '</div>', {
            compiled: true,
            disableFormats: true
        }
    ],

    listConfig: {
        itemTpl: [
            '<div style="font-size:120%; padding:7px"><i class="{icon}"></i> {text}&nbsp;</div>',
        ]
    },

    setRawValue: function (value) {
        var me = this;
        me.rawValue = value;
        if (me.inputEl) {
            me.inputEl.dom.innerHTML = value;
        }
        return value;
    }
})